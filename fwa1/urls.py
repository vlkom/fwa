from django.conf.urls import url
from fwa1.apps.welcome.views import index

urlpatterns = [
    url('^$', index),
    url('^(?P<path>.*)/$', index),
]
